<?php
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

ini_set('memory_limit', '256M');
ini_set('display_errors', 1);
ini_set('max_execution_time', 6000);

function createRuleCommunity($storeIds, $fromUrl, $toUrl)
{
    // Create rewrite:
    /** @var Mage_Core_Model_Url_Rewrite $rewrite */
    $rewrite = Mage::getModel('core/url_rewrite');
 
    // Check for existing rewrites:
    foreach($storeIds as $storeId)
    {
    echo "Creating rule [$fromUrl] - [$toUrl] for store [$storeId]". PHP_EOL;
        // Attempt loading it first, to prevent duplicates:
        $rewrite->loadByIdPath($fromUrl);
 
        $rewrite->setStoreId($storeId);
        $rewrite->setOptions('RP');
        $rewrite->setIdPath($fromUrl);
        $rewrite->setRequestPath($fromUrl);
        $rewrite->setIsSystem(0);
        $rewrite->setTargetPath($toUrl);
 
        $rewrite->save();
    }
}
/**************************************************************************/
/**************************************************************************/
/**************************************************************************/
$allStores = Mage::app()->getStores();
$allStoreIds = [];
foreach ($allStores as $_eachStoreId => $val) 
{
    $allStoreIds[] = Mage::app()->getStore($_eachStoreId)->getId();
}

$inputFile = new SplFileObject($argv[1]);
$inputFile->setFlags(SplFileObject::READ_CSV);

$resultFile = new SplFileObject('result-' . array_shift(explode('.',$argv[1])) . '.csv', 'w');

if (false)
foreach ($inputFile as $dataArray)
{
	//Skip emply lines.
	if ('' == implode('', $dataArray)) continue;
	
    $error = '';
    
    $product = Mage::getSingleton('catalog/product')->loadByAttribute('sku', $dataArray[0]);
    if (is_object($product)) {
        createRuleCommunity($allStoreIds, $dataArray[1], $product->getUrlPath());
    } else {
        echo "Product: $dataArray[0] - $dataArray[1]";
        echo " Not found." . PHP_EOL;
        $error = 'Not found';
    }
	array_unshift($dataArray, "* $error");
	$resultFile->fputcsv($dataArray);
    
}

createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "2171", 'kabel/kabel-hdmi');
createRuleCommunity($allStoreIds, "2127", 'kabel/kabel-hdmi');
createRuleCommunity($allStoreIds, "2227", 'hdmi-oborudovanie/hdmi-splittery-razvetviteli');
createRuleCommunity($allStoreIds, "1731", 'kabel/kabel-hdmi-dvi');
createRuleCommunity($allStoreIds, "5028", 'kabel/kabel-hdmi-dvi');
createRuleCommunity($allStoreIds, "2328", 'kabel/kabel-3-5mm-stereo');
createRuleCommunity($allStoreIds, "1050", 'kabel/kabel-scart');
createRuleCommunity($allStoreIds, "1141", 'kabel/kabel-rca');
createRuleCommunity($allStoreIds, "1692", 'kabel/kabel-usb');
createRuleCommunity($allStoreIds, "2146", 'kabel/kabel-usb');
createRuleCommunity($allStoreIds, "2101", 'kabel/kabel-vitaja-para-patch-kord');
createRuleCommunity($allStoreIds, "2453", 'kabel/kabel-samsung-galaxy-tab');
createRuleCommunity($allStoreIds, "2526", 'kabel/kabel-vga-vga-dvi-dvi');
createRuleCommunity($allStoreIds, "1653", 'kabel/kabel-apple');
createRuleCommunity($allStoreIds, "1614", 'kabel/kabel-firewire');
createRuleCommunity($allStoreIds, "1575", 'kabel/kabel-opticheskij');
createRuleCommunity($allStoreIds, "2118", 'kabel/kabel-antennyj');
createRuleCommunity($allStoreIds, "2247", 'kabel/kabel-akusticheskij');
createRuleCommunity($allStoreIds, "1536", 'hdmi-oborudovanie/hdmi-adaptery-i-perehodniki');
createRuleCommunity($allStoreIds, "4264", 'zarjadnye-ustrojstva');
createRuleCommunity($allStoreIds, "1083", 'tumby-pod-televizory');
createRuleCommunity($allStoreIds, "1085", 'tumby-pod-televizory');
createRuleCommunity($allStoreIds, "1527", 'tumby-pod-televizory');
createRuleCommunity($allStoreIds, "4473", 'tumby-pod-televizory/tumby-pod-tv-uglovye');
createRuleCommunity($allStoreIds, "2319", 'tumby-pod-televizory/tumby-s-kronshtejnom');
createRuleCommunity($allStoreIds, "3990", 'prezentacionnye-stojki');
createRuleCommunity($allStoreIds, "1203", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr');
createRuleCommunity($allStoreIds, "1206", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizorov-i-monitorov-do-28');
createRuleCommunity($allStoreIds, "2992", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-29-40');
createRuleCommunity($allStoreIds, "3040", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-41-55');
createRuleCommunity($allStoreIds, "3069", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-56-100');
createRuleCommunity($allStoreIds, "5487", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-monitorov-nastol-nye-15-27');
createRuleCommunity($allStoreIds, "2969", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-monitorov-nastol-nye-15-27');
createRuleCommunity($allStoreIds, "1279", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-29-40');
createRuleCommunity($allStoreIds, "1292", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-41-55');
createRuleCommunity($allStoreIds, "1427", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-56-100');
createRuleCommunity($allStoreIds, "4360", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr');
createRuleCommunity($allStoreIds, "2411", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/stojki-derzhateli-pod-planshety');
createRuleCommunity($allStoreIds, "2161", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-proektorov-videoproektorov');
createRuleCommunity($allStoreIds, "2310", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-televizora-potolochnye');
createRuleCommunity($allStoreIds, "2261", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-kolonok');
createRuleCommunity($allStoreIds, "2315", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-motorizirovannye');
createRuleCommunity($allStoreIds, "2082", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-dvd-dvd-tjunerov');
createRuleCommunity($allStoreIds, "2475", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/kreplenija-dlja-mikrovolnovok-svch');
createRuleCommunity($allStoreIds, "4073", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/aksessuary-k-kreplenijam');
createRuleCommunity($allStoreIds, "5349", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/aksessuary-k-kreplenijam');
createRuleCommunity($allStoreIds, "3212", 'hdmi-oborudovanie');
createRuleCommunity($allStoreIds, "3213", 'hdmi-oborudovanie/hdmi-splittery-razvetviteli');
createRuleCommunity($allStoreIds, "3512", 'hdmi-oborudovanie/hdmi-svitchi-perekljuchateli-kommutatory');
createRuleCommunity($allStoreIds, "3222", 'hdmi-oborudovanie/hdmi-konvertery');
createRuleCommunity($allStoreIds, "3217", 'hdmi-oborudovanie/hdmi-matricy-matrichnye-perekljuchateli');
createRuleCommunity($allStoreIds, "4894", 'hdmi-oborudovanie/hdmi-udliniteli-i-repitery');
createRuleCommunity($allStoreIds, "5246", 'hdmi-oborudovanie/vstraivaemye-produkty');
createRuleCommunity($allStoreIds, "4876", 'hdmi-oborudovanie/hdmi-adaptery-i-perehodniki');
createRuleCommunity($allStoreIds, "2433", 'jekrany-dlja-proektorov-proekcionnye-jekrany');
createRuleCommunity($allStoreIds, "3640", 'jekrany-dlja-proektorov-proekcionnye-jekrany');
createRuleCommunity($allStoreIds, "3641", 'jekrany-dlja-proektorov-proekcionnye-jekrany');
createRuleCommunity($allStoreIds, "3639", 'jekrany-dlja-proektorov-proekcionnye-jekrany');
createRuleCommunity($allStoreIds, "2578", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/aksessuary-k-kreplenijam');
createRuleCommunity($allStoreIds, "2598", 'kronshtejny-dlja-televizora-dvd-tjunerov-proektorov-i-dr/aksessuary-k-kreplenijam');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');
createRuleCommunity($allStoreIds, "597", 'kabel');

echo "Bye\n";
