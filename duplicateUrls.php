<?php
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

ini_set('display_errors', 1);
ini_set('max_execution_time', 600);

$maxRowsToLog = 10;
        try {
            $counter = 0;
            $logMessage = '';
            $start = time();
            $storeId = Mage::app()->getStore()->getId() . PHP_EOL;
 
            //url key attriubte load for further use
            $entityType = Mage::getModel('eav/entity_type')->loadByCode('catalog_product');
            $attributes = $entityType->getAttributeCollection()
                ->addFieldToFilter('attribute_code', array('eq' => 'url_key'))
            ;
            $urlKeyAttribute = $attributes->getFirstItem();
            $urlKeyAttributeTable = $attributes->getTable($entityType->getEntityTable());
 
            //loading collection with number of duplicated url keys
            $duplicatesCollection = Mage::getModel('catalog/product')->getCollection();
            $duplicatesCollection->getSelect()
                ->joinLeft(
                    array('url_key' => $urlKeyAttributeTable . '_' . $urlKeyAttribute->getBackendType()),
                    'e.entity_id' . ' = url_key.entity_id AND url_key.attribute_id = ' . $urlKeyAttribute->getAttributeId() . ' AND url_key.store_id = ' . $storeId,
                    array($urlKeyAttribute->getAttributeCode() => 'url_key.value')
                )
                ->columns(array('duplicates_calculated' => new Zend_Db_Expr ('COUNT(`url_key`.`value`)')))
                ->group('url_key.value')
                ->order('duplicates_calculated DESC')
            ;
 
            foreach($duplicatesCollection as $item) {
                if($item->getData('duplicates_calculated') > 1) {
                    //loading product ids with duplicated url keys
                    $duplicatedUrlKey = $item->getData('url_key');
                    $productCollection = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToSelect('url_key')
                        ->addAttributeToFilter('url_key', array('eq' => $duplicatedUrlKey))
                    ;
                    $ids = $productCollection->getAllIds();
 
                    foreach($ids as $id){
                        try {
                            //update product url key
                            $product = Mage::getModel('catalog/product')->load($id);
                            $sku = $product->getData('sku');
                            $urlKey = $product->getData('url_key');
                            $product->setData('url_key', $urlKey . '-' . strtolower(str_replace(' ', '-', $sku)));
                            $product->save();
                            $counter++;
                            $message = 'Product id# ' . $product->getId() . ' "' . $product->getName() . '" ' . ' url key was changed from "' . $urlKey . '" to "' . $product->getData('url_key') . '"' . PHP_EOL;
                            $logMessage .= $message;
                            //log will be update with the packs of messages
                            if($counter % $maxRowsToLog == 0) {
                                Mage::log($logMessage, null, 'atwix_rewrites_doctor.log', true);
                                $logMessage = '';
                            }
                            echo $message;
                        } catch (Exception $e) {
                            echo $e->getMessage() . PHP_EOL;
                            Mage::log($e->getMessage(), null, 'atwix_rewrites_doctor.log', true);
                        }
                    }
                } else {
                    //we will break the cycle after all duplicates in query were processed
                    break;
                }
            }
 
            if($counter % $maxRowsToLog != 0) {
                Mage::log($logMessage, null, 'atwix_rewrites_doctor.log', true);
            }
 
            $message = $counter . ' products were updated';
            Mage::log($message, null, 'atwix_rewrites_doctor.log', true);
            echo $message . PHP_EOL;
 
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            Mage::log($e->getMessage(), null, 'atwix_rewrites_doctor.log', true);
        }


echo "Bye\n";
