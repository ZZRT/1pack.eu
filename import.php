<?php
/*
 * 
 * 
 * 
SELECT
	(
	SELECT GROUP_CONCAT( value SEPARATOR  ', ' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS sku
		WHERE sku.contentid = content.id
		AND sku.tmplvarid = 5
	) AS sku,
	content.pageTitle,
	(
	SELECT GROUP_CONCAT( value SEPARATOR  ', ' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS brand
		WHERE brand.contentid = content.id
		AND brand.tmplvarid = 17
	) AS brand,
 	(
	SELECT cnt.pageTitle
		FROM  `modx_site_content` AS cnt
		WHERE cnt.id = content.parent
	) AS parent_name,
	(
	SELECT GROUP_CONCAT( value SEPARATOR  ', ' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS price
		WHERE price.contentid = content.id
		AND price.tmplvarid = 7
	) AS price,
	(
	SELECT GROUP_CONCAT( value SEPARATOR  'LOLIPOP ' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS oldPrice
		WHERE oldPrice.contentid = content.id
		AND oldPrice.tmplvarid = 25
	) AS oldPrice,
	content.published as in_stock,
	content.description,
	content.content,
	(
	SELECT GROUP_CONCAT( concat('photos/', filename) SEPARATOR  ',' ) 
		FROM  `modx_gallery` AS galmain
		WHERE galmain.content_id = content.id AND galmain.index = 1
	) AS mainimages,
	(
	SELECT GROUP_CONCAT( concat('photos/', filename) SEPARATOR  ',' ) 
		FROM  `modx_gallery` AS gal
		WHERE gal.content_id = content.id AND gal.index <> 1
	) AS images
FROM  `modx_site_content` AS content
WHERE content.deleted = 0
having price is not null and sku is not null
ORDER BY sku, in_stock desc;

Was used sometime ago:
1) AND content.id = 5325
2) WHERE content.deleted = 0 AND content.published = 1



	(
	SELECT GROUP_CONCAT( value SEPARATOR  'LOLIPOP ' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS oldPrice
		WHERE oldPrice.contentid = content.id
		AND oldPrice.tmplvarid = 25
	) AS oldPrice,

	(
	SELECT GROUP_CONCAT( value SEPARATOR  ',' ) 
		FROM  `modx_site_tmplvar_contentvalues` AS cv1
		WHERE cv1.contentid = content.id
		AND (
		cv1.tmplvarid =1
		OR cv1.tmplvarid =13
		OR cv1.tmplvarid =14
		OR cv1.tmplvarid =15
		OR cv1.tmplvarid =16
		)
	) AS images
 */
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

ini_set('display_errors', 1);
ini_set('max_execution_time', 7100);

define(SKU_IDX,			0);
define(NAME_IDX,		1);
define(BRAND_IDX,		2);
define(CATEGORY_IDX,	3);
define(PRICE_IDX,		4);
define(OLD_PRICE_IDX,	5);
define(IN_STOCK_IDX,	6);
define(SHORT_DESCR_IDX,	7);
define(LONG_DESCR_IDX,	8);
define(MAINIMAGES_IDX,	9);
define(IMAGES_IDX,		10);

$brands = [
	strtoupper("TechLink"),
	strtoupper("ITech"),
	strtoupper("Brateck"),
	strtoupper("ProLink"),
	strtoupper("Bandridge"),
	strtoupper("Vogel's"),
	strtoupper("LOGAN"),
	strtoupper("TechPoint"),
	strtoupper("OmniMount"),
	strtoupper("КВАДО"),
	strtoupper("Ultra"),
	strtoupper("VIVANCO"),
	strtoupper("Electriclight"),
];

$categoryMergeRules = [
	'Кабель HDMI - mini, micro HDMI' => 'Кабель HDMI',
	'Портативные зарядные устройства' => 'Зарядные устройства',
	'Тумбы под ТВ 26"-39" (для LCD(ЖК), плазменных ТВ)' => "Тумбы под ТВ",
	'Тумбы под ТВ 40"-84" (для LCD(ЖК), плазменных ТВ)' => "Тумбы под ТВ",
	'Крепления для телевизора 15"-24" (пристенные)' => 'Крепления для телевизоров и мониторов до 28"',
	'Крепления (с выносом) для ТВ, монитора 15"-24"' => 'Крепления для телевизоров и мониторов до 28"',
	'Крепления для телевизора 26"-39" (пристенные)' => 'Крепления для телевизора 29"-40"',
	'Крепления (с выносом) для ТВ 26"-39" (ЖК,LED,плазменных)' => 'Крепления для телевизора 29"-40"',
	'Крепления для телевизора 40"-50" (пристенные)' => 'Крепления для телевизора 41"-55"',
	'Крепления (с выносом) для ТВ 40"-50" (ЖК,LED,плазменных)' => 'Крепления для телевизора 41"-55"',
	'Крепления для телевизора 51"-100" (пристенные)' => 'Крепления для телевизора 56"-100"',
	'Крепления (с выносом) для ТВ 51"-100" (ЖК,LED,плазменных)' => 'Крепления для телевизора 56"-100"',
	'Кабель-каналы' => 'Аксессуары к креплениям',
	'Очищающие системы' => 'Аксессуары к креплениям',
	'Держатели, стойки под Ipad' => 'Стойки, держатели под планшеты',
	'Экраны настенные' => 'Экраны для проекторов',
	'Экраны напольные' => 'Экраны для проекторов',
	'Экраны на треноге' => 'Экраны для проекторов',
	'Экраны моторизированные' => 'Экраны для проекторов',
	'Кабель USB - mini, micro USB' => 'Кабель USB',
	'HDMI сплиттеры (разветлители)' => 'HDMI сплиттеры (разветвители)',
	'Кабель и аксессуары к Apple' => 'Кабель Apple',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
];

function normalize($rawStr)
{
	//return str_replace([' ', "\n", "\t", "\r"], '', $rawStr);
	return $rawStr;
}

/******************************************************************************/
class Sku {
	
	private $sku;
	
	function __construct()
	{
		$this->sku = "";
	}
	
	function reload($dataArray)
	{
		if(0 < strlen($dataArray[SKU_IDX])) {
			$this->sku = normalize($dataArray[SKU_IDX]);
		}
		return $this;
	}
	
	function anotherSkuDetected($dataArray)
	{
		$result = false;
		$normalizedSku = normalize($dataArray[SKU_IDX]);
		
		if(0 < strlen($normalizedSku)) {
			$result = ($this->sku != $normalizedSku);
		}
		return $result;
	}
	
	function getSku()
	{
		return $this->sku;
	}
	
}

/******************************************************************************/
class Product {
	private $errors;
	private $warnings;
	
	public $sku;
	private $isInStock;
	private $categories;
	private $images;

	private $name;
	private $shortDescr;
	private $longDescr;
	private $msrp_price;
	private $price;
	private $oldPrice;
	private $brand;

	private $isJustCreated;
	public $npp;
	
	function __construct()
	{
		$this->errors = [];
		$this->warnings = [];
		$this->isInStock = false;
		$this->categories = [];
		$this->images = [];
		
		$this->isJustCreated = true;
		$this->sku = new Sku();

		$this->name = '';
		$this->shortDescr = '';
		$this->longDescr = '';
		$this->msrp_price = '';
		$this->price = '';
		$this->oldPrice = '';
		$this->brand = '';
	}
	
	public function __get($var)
	{
		return $this->$var;
	}
	
	function addError($error)
	{
		if(strlen($error) > 0) $this->errors[] = "Error: $error";
		
		return $this;
	}
	
	function addWarning($warning)
	{
		if(strlen($warning) > 0) $this->warnings[] = "Warning: $warning";
		
		return $this;
	}
	
	function resetWarnings()
	{
		$this->warnings = [];
		
		return $this;
	}
	
	function reset($dataArray, $npp)
	{
		$this->errors = [];
		$this->warnings = [];
		$this->isInStock = false;
		$this->categories = [];
		$this->images = [];

		$this->isJustCreated = false;
		$this->sku->reload($dataArray);
		
		$this->npp = sprintf('%08d', $npp);

		$this->name = '';
		$this->shortDescr = '';
		$this->longDescr = '';
		$this->msrp_price = '';
		$this->price = '';
		$this->oldPrice = '';
		$this->brand = '';

		return $this;
	}
	
	function recognizePrice($rawPrice)
	{
		return floatval(str_replace(',', '.', normalize($rawPrice)));
	}
	
	function loadNextCsvLine($dataArray)
	{
		$_isInStock = ('1' == trim($dataArray[IN_STOCK_IDX]));
		if ($this->isInStock && !$_isInStock) {
			$this->addWarning('Ignoring not-in-stock sku duplicate after in-stock one');
		} else {
			$this->isInStock = $_isInStock;
			if (!$this->isInStock) $this->addWarning('Out-of-stock product.');
			$this
				->setGivenField('name', trim($dataArray[NAME_IDX]))
				->setGivenField('shortDescr', trim($dataArray[SHORT_DESCR_IDX]))
				->setGivenField('longDescr', trim($dataArray[LONG_DESCR_IDX]))
				->setGivenField('price', trim($dataArray[PRICE_IDX]))
				->setGivenField('oldPrice', trim($dataArray[OLD_PRICE_IDX]))
				->setBrand(strtok(trim($dataArray[BRAND_IDX]), " "))
				->appendCategory(trim($dataArray[CATEGORY_IDX]))
				->appendImages(trim($dataArray[MAINIMAGES_IDX]))
				->appendImages(trim($dataArray[IMAGES_IDX]))
			;
		}		
		return $this;
	}
	
	function setGivenField($fieldName, $value)
	{
		if (0 == strlen($value)) return $this;
		if ('NULL' == strtoupper($value)) return $this;
		if ($this->$fieldName == $value) return $this;
		
		if (0 == strlen($this->$fieldName)) {
			$this->$fieldName = $value;
			//echo "$fieldName set to [$value]" . PHP_EOL;
		} else {
			$this->addWarning("Wrong $fieldName duplicate!");
		}
		
		return $this;
	}
	
	function appendCategory($categoryName)
	{
		global $categoryMergeRules;
		//echo "Looking for category [$categoryName]\n";
		$_realCategoryName = array_key_exists($categoryName, $categoryMergeRules) ? $categoryMergeRules[$categoryName] : $categoryName;
		$_id = Mage::getResourceModel('catalog/category_collection')
				->addFieldToFilter('name', $_realCategoryName)
				->getFirstItem()
				->getId();
				
		if (is_numeric($_id)) {
			if (!in_array($_id, $this->categories)) $this->categories[] = $_id;
		} else {
			$this->addError("Can not find category [$_realCategoryName]");
		}
		
		return $this;
	}
	
	function appendImages($imgs)
	{
		$_imageNames = explode(',', $imgs);
		foreach ($_imageNames as $_nextImageName)
		{
			if ('' != trim($_nextImageName)) {
				if (!file_exists('media/import/' . $_nextImageName)) {
					$this->addWarning("Image file missing: $_nextImageName");
					//break;
				} else {
					if (!in_array($_nextImageName, $this->images)) $this->images[] = $_nextImageName;
				}
			}
		}
		return $this;
	}
	
	function setBrand($brand)
	{
		global $brands;
		
		$this->brand = ('NULL' == $brand) ? '' : $brand;

		if (0 == strlen($this->brand))
		if (0 != strlen($this->name)) {
			// Let's try to deduce brand from the name.
			$namePieces = explode(' ', strtoupper(str_replace([',', "\"", "'", "\n", "\t", "\r"], ' ', $this->name)));
			foreach($namePieces as $nextPiece)
			{
				if (in_array($nextPiece, $brands)) {
					$this->brand = $nextPiece;
					break;
				}
			}
		}
		
		if (0 != strlen($this->brand)) {
			$this->appendCategory($this->brand);
		} else {
			$this->addWarning("Empty brand.");
		}
		
		return $this;
	}
	
	function doFinalChecks()
	{
		// Let's check for errors.
		if (0 == strlen($this->name)) $this->addError("Empty name!");
		if (0 == strlen($this->longDescr)) $this->addError("Empty longDescr!");
		//if (0 == $this->msrp_price) $this->addError("Empty msrp price!");
		if (0 == $this->recognizePrice($this->price)) $this->addError("Empty price!");
		
		return $this;
	}
	
	function isPerfectlyValid()
	{
		$this->doFinalChecks();
		return (0 == count($this->errors));
	}
	
	/*****************************/
	/*****************************/
	/*****************************/
	function flushToDatabase()
	{
		if (!$this->isJustCreated)
		if (!$this->isPerfectlyValid()) {
			echo "Skipping invalid objects group. Common sku=[{$this->sku->getSku()}]" . PHP_EOL;
echo implode(' - ', $this->errors) . PHP_EOL;
		} else {

			$getShortFileName = function ($longFileName)
				{
					return str_replace([' ', "\n", "\t", "\r"], '__', array_pop(explode('/', $longFileName)));
				};

			$_price = $this->recognizePrice($this->price);
			$_specialPrice = '';
			if (0 < $this->recognizePrice($this->oldPrice)) {
				$_specialPrice = $_price;
				$_price = $this->recognizePrice($this->oldPrice);
			}
			
			$_importArray = [];
			$_importArray[] =
				[
					'sku' => $this->sku->getSku(),
					'_type' => 'simple',
					'_attribute_set' => 'techtochka',
					'_product_websites' => 'base',
					'_category' => $this->categories,
					//'msrp' => $this->oneColorProductBunches[0]->msrp_price,
					'price' => $_price,
					'special_price' => $_specialPrice,
					'name' => $this->name,
					'description' => preg_replace(['/\<a.*?a\>/i', '/\<img.*?\>/i'],'',$this->longDescr),
					'short_description' => $this->shortDescr,
					'weight' => 1,
					'status' => 1,
					'visibility' => 4,
					'is_in_stock' => $this->isInStock ? 1 : 0,
					'qty' => 9999,
					//'_super_products_sku' => $this->getAllSimpleProductsSkus(),
					//'_super_attribute_code' => ['color', 'size'],
					//'small_image'				=> array_map($getShortFileName, $this->images)[0],
					'small_image'				=> $this->images[0],
					'image'						=> $this->images[0],
					'thumbnail'					=> $this->images[0],
					'_media_image'				=> $this->images,
					'_media_target_filename' => array_map($getShortFileName, $this->images),
					'_media_lable' => array_map($getShortFileName, $this->images),
					'_media_position' => array_keys($this->images),
					//'news_from_date' => '2015-08-23 00:00:00',
					'default_category' => $this->categories[0],
					'tax_class_id' => 0,
					'brand' => $this->brand,
					/*
					'npp' => $this->npp,
					'_tier_price_website' => ['all'],
					'_tier_price_customer_group' => ['all'],
					'_tier_price_qty' => [3],
					'_tier_price_price' => [$this->oneColorProductBunches[0]->price * 0.8],
					*/
				];
			
			//print_r($_importArray);
			echo "=================================== Importing {$this->sku->getSku()} ..." . PHP_EOL;
			
			//if(false)
			try {
				/** @var $import AvS_FastSimpleImport_Model_Import */
				$import = Mage::getModel('fastsimpleimport/import');
				$import
					->setUseNestedArrays(true)
					->processProductImport($_importArray);
			} catch (Exception $e) {
				//$this->addError($e->getMessage());
				foreach($import->getErrorMessages() as $_nextError => $_unusefulArray)
				{
					$this->addError($_nextError);
					echo $_nextError;
				}
			}

		}
		return $this;
	}
	
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
$inputFile = new SplFileObject($argv[1]);
$inputFile->setFlags(SplFileObject::READ_CSV);

$resultFile = new SplFileObject('result-' . array_shift(explode('.',$argv[1])) . '.csv', 'w');

$currentProduct = new Product();

$npp = 999999;	// Just a product SKU's counter.
//while ($nextString = $inputFile->fgetcsv())
	//$dataArray = preg_split("/\t/", $nextString);
	//$dataArray = explode("\t", $nextString);
foreach ($inputFile as $dataArray)
{
	//Skip emply lines.
	if ('' == implode('', $dataArray)) continue;
	
	//Skip unneeded categories.
	if ('HDMI свитчеры, сплиттеры, преобразователи' == $dataArray[CATEGORY_IDX]) continue;
	if ('Крепления для телевизора белые (ТВ,ЖК,LED)' == $dataArray[CATEGORY_IDX]) continue;
	
	if ($currentProduct->sku->anotherSkuDetected($dataArray)) {
		// We must flush current product to database and reset it (product, not database :)).
		$currentProduct->flushToDatabase();
		if (0 < count($currentProduct->errors)) {
			//$resultFile->fwrite('IMPORT TIME ERROR: ' . implode(' - ', $currentProduct->errors) . "\t" . PHP_EOL);
			$resultFile->fputcsv(['IMPORT TIME ERROR: ' . implode(' - ', $currentProduct->errors)]);
			echo $currentProduct->errors;
		}
		$currentProduct->reset($dataArray, $npp--);
	}
	
	// Let's start to really process the next string.
	// At this stage $currentProduct either freshly reset or already contain some (or just one) categories.
	$firstGlanceError = "";
	try {
		$currentProduct->loadNextCsvLine($dataArray);
	} catch(Exception $ex) {
		$firstGlanceError = $ex->getMessage();
	}
	
	array_unshift($dataArray, "* $firstGlanceError" . implode(' - ', $currentProduct->warnings) );
	$resultFile->fputcsv($dataArray);
	$currentProduct->resetWarnings();
}

// Final flush.
$currentProduct->flushToDatabase();
if (0 < count($currentProduct->errors)) {
	//$resultFile->fwrite('IMPORT TIME ERROR: ' . implode(' - ', $currentProduct->errors) . "\t" . PHP_EOL);
	$resultFile->fputcsv(['IMPORT TIME ERROR: ' . implode(' - ', $currentProduct->errors)]);
}

echo "Bye\n";
