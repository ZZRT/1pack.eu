<?php
define('DS', DIRECTORY_SEPARATOR);

function _getExtractSchemaStatement($sqlFileName, $db)
{
    $dumpSchema = 'mysqldump' . ' ';
    $dumpSchema .= '--no-data' . ' ';
    $dumpSchema .= '-u ' . $db['user'] . ' ';
    if(!empty($db['pass']))
        $dumpSchema .= '-p' . $db['pass'] . ' ';
    $dumpSchema .= '-h ' . $db['host'] . ' ';
    $dumpSchema .= $db['name'] .' > ' . $sqlFileName;

    return $dumpSchema;
}

function _getExtractDataStatement($sqlFileName, $db)
{
    $tables = array(
        'cron_schedule',
        'captcha_log',
        'adminnotification_inbox',
        'aw_core_logger',
        'dataflow_batch_export',
        'dataflow_batch_import',
        'log_customer',
        'log_quote',
        'log_summary',
        'log_summary_type',
        'log_url',
        'log_url_info',
        'log_visitor',
        'log_visitor_info',
        'log_visitor_online',
        'index_event',
        'report_event',
        'report_viewed_product_index',
        'report_compared_product_index',
        'catalog_compare_item',
        'catalogindex_aggregation',
        'catalogindex_aggregation_tag',
        'catalogindex_aggregation_to_tag',
        'cataloginventory_stock_status_idx',
        'catalog_category_anc_categs_index_idx',
        'catalog_category_anc_products_index_idx',
        'catalog_category_product_index_enbl_idx',
        'catalog_category_product_index_idx',
        'catalog_product_index_eav_decimal_idx',
        'catalog_product_index_eav_idx',
        'catalog_product_index_price_bundle_idx',
        'catalog_product_index_price_bundle_opt_idx',
        'catalog_product_index_price_bundle_sel_idx',
        'catalog_product_index_price_cfg_opt_agr_idx',
        'catalog_product_index_price_cfg_opt_idx',
        'catalog_product_index_price_downlod_idx',
        'catalog_product_index_price_final_idx',
        'catalog_product_index_price_idx',
        'catalog_product_index_price_opt_agr_idx',
        'catalog_product_index_price_opt_idx',
        'cataloginventory_stock_status_tmp',
        'catalog_category_anc_categs_index_tmp',
        'catalog_category_anc_products_index_tmp',
        'catalog_category_product_index_enbl_tmp',
        'catalog_category_product_index_tmp',
        'catalog_product_index_eav_decimal_tmp',
        'catalog_product_index_eav_tmp',
        'catalog_product_index_price_bundle_opt_tmp',
        'catalog_product_index_price_bundle_sel_tmp',
        'catalog_product_index_price_bundle_tmp',
        'catalog_product_index_price_cfg_opt_agr_tmp',
        'catalog_product_index_price_cfg_opt_tmp',
        'catalog_product_index_price_downlod_tmp',
        'catalog_product_index_price_final_tmp',
        'catalog_product_index_price_opt_agr_tmp',
        'catalog_product_index_price_opt_tmp',
        'catalog_product_index_price_tmp'
    );

    $ignoreTables = ' ';
    foreach($tables as $table) {
        $ignoreTables .= '--ignore-table=' . $db['name'] . '.' . $db['pref'] . $table . ' ';
    }

    $dumpData = 'mysqldump' . ' ';
    $dumpData .= $ignoreTables;
    $dumpData .=  '-u ' . $db['user'] . ' ';
    if(!empty($db['pass']))
        $dumpData .= '-p' . $db['pass'] . ' ';
    $dumpData .= '-h ' . $db['host'] . ' ';
    $dumpData .= $db['name'] .' >> ' . $sqlFileName;

    return $dumpData;
}

function export_tiny()
{
    $configPath = '.' . DS . 'app' . DS . 'etc' . DS . 'local.xml';
    if (!file_exists ($configPath))
    {
        die("Can't find local.xml file. Place this php script file to root Magento directory and try again.");
    }
    $xml = simplexml_load_file($configPath, NULL, LIBXML_NOCDATA);

    $db['host'] = $xml->global->resources->default_setup->connection->host;
    $db['name'] = $xml->global->resources->default_setup->connection->dbname;
    $db['user'] = $xml->global->resources->default_setup->connection->username;
    $db['pass'] = $xml->global->resources->default_setup->connection->password;
    $db['pass'] = $db['pass']->__toString();
    $db['pref'] = $xml->global->resources->db->table_prefix;

    $sqlFileName =  'var' . DS . $db['name'] . '-' . date('j-m-y-h-i-s') . '.sql';

    //Extract the DB schema
    $dumpSchema = _getExtractSchemaStatement($sqlFileName, $db);
    if(!empty($db['pass']))
        $querySchema = str_replace ('-p' . $db['pass'], '-p***', $dumpSchema);
    else
        $querySchema = $dumpSchema;
    echo $querySchema;
    exec ($dumpSchema);

    //Extract the DB data
    $dumpData = _getExtractDataStatement($sqlFileName, $db);
    if(!empty($db['pass']))
        $queryData = str_replace ('-p' . $db['pass'], '-p***', $dumpData);
    else
        $queryData = $dumpData;
    echo '<br/><br/>' . $queryData;
    exec ($dumpData);

    echo '<br/><br/>';

    if (file_exists ($sqlFileName))
    {
        echo 'SQL file created';
        archiving ($sqlFileName);
    }
    else
    {
        echo 'Error, sql file don`t created';
    }

}


function archiving($sqlFileName)
{
    $zip = new ZipArchive();
    $filename = str_replace ('.sql', '.zip', $sqlFileName);

    if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
        exit("\nCan't create zip file");
    }

    $zip->addFile($sqlFileName, str_replace ('var' . DS, DS, $sqlFileName));
    $zip->close();
}

try{
    export_tiny();
} catch (Exception $e) {
    echo $e->getMessage();
}