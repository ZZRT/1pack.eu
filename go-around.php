<?php
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

ini_set('display_errors', 1);
ini_set('max_execution_time', 600);


$collection = Mage::getResourceModel('catalog/product_collection');
//            ->addAttributeToSelect('*')
//            ->addAttributeToFilter('type_id','configurable'); 

$stores = array_keys(Mage::app()->getStores());

foreach($collection as $_bare_product){


	$productId = $_bare_product->getId();
	//load the product
	$product = Mage::getModel('catalog/product')->load($productId);
	echo "Processing product with id=$productId [{$product->getName()}]" . PHP_EOL;
    
    $stockItem = $product->getStockItem();
    if (!$stockItem->getIsInStock()) {
        echo "Not in stock! Setting as disabled...";
        foreach($stores as $storeId) {
            Mage::getModel('catalog/product_status')->updateProductStatus($product->getId(), $storeId, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
        }
    }
    /*
	//get all images
	$mediaGallery = $product->getMediaGallery();
	//if there are images
	if (isset($mediaGallery['images'])){
		//loop through the images
		$counter = 0;
		foreach ($mediaGallery['images'] as $image){
			$counter++;
			//set the first image as the base image
			if (1 == $counter) {
				Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('image'=>$image['file'], 'small_image'=>$image['file'], 'thumbnail'=>$image['file']), 0);
			} elseif (2 == $counter) {
				Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('small_image'=>$image['file']), 0);
			} else {
				break;
			}
		}
	}
    */
    
}

echo "Bye\n";
