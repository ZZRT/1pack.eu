<?php

class Mageplaza_ActionsList_Model_Adminhtml_Observer{

    public function scheduledGenerateSitemaps()
    {
        Mage::getModel('mageplaza_actionslist/sitemap')->generateXml();
    }
}