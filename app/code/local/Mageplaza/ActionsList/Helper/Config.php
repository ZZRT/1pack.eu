<?php
class Mageplaza_ActionsList_Helper_Config extends Mage_Core_Helper_Abstract
{

    const DEFAULT_URL = 'actions';
    /**
     * get general config by code
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */
    public function getGeneralConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/general/' . $code, $store);
    }


    /**
     * get actions url
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */
    public function getActionsUrl($store = null)
    {
        return Mage::getUrl($this->getActionsRoute(), array('_store' => $store));
    }

    public function getActionsRoute()
    {
        return self::DEFAULT_URL;
    }



    /**
     * get post config by code
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */

    public function getPostConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/post/' . $code, $store);
    }

    /**
     * get comment config by code
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */

    public function getCommentConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/comment/' . $code, $store);
    }

    /**
     * get category config by code
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */

    public function getCategoryConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/category/' . $code, $store);
    }

    /**
     * get tags config by code
     *
     * @param      $code
     * @param null $store
     * @return mixed
     */

    public function getTagConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/tag/' . $code, $store);
    }

    /**
     * get sidebar config
     * @param $code
     * @param null $store
     * @return mixed
     */
    public function getSidebarConfig($code, $store = null)
    {
        return Mage::getStoreConfig('mageplaza_actionslist/sidebar/' . $code, $store);
    }













}