<?php
/**
 * Mageplaza_ActionsList extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Mageplaza
 * @package        Mageplaza_ActionsList
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tag admin block
 *
 * @category    Mageplaza
 * @package     Mageplaza_ActionsList
 * @author      Sam
 */
class Mageplaza_ActionsList_Block_Adminhtml_Tag extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Sam
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_tag';
        $this->_blockGroup         = 'mageplaza_actionslist';
        parent::__construct();
        $this->_headerText         = Mage::helper('mageplaza_actionslist')->__('Tag');
        $this->_updateButton('add', 'label', Mage::helper('mageplaza_actionslist')->__('Add Tag'));

    }
}
