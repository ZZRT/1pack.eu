<?php
/**
 * Mageplaza_ActionsList extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Mageplaza
 * @package        Mageplaza_ActionsList
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Tag admin edit tabs
 *
 * @category    Mageplaza
 * @package     Mageplaza_ActionsList
 * @author      Sam
 */
class Mageplaza_ActionsList_Block_Adminhtml_Tag_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Sam
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('tag_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('mageplaza_actionslist')->__('Tag'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Mageplaza_ActionsList_Block_Adminhtml_Tag_Edit_Tabs
     * @author Sam
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_tag',
            array(
                'label'   => Mage::helper('mageplaza_actionslist')->__('Tag'),
                'title'   => Mage::helper('mageplaza_actionslist')->__('Tag'),
                'content' => $this->getLayout()->createBlock(
                    'mageplaza_actionslist/adminhtml_tag_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_tag',
            array(
                'label'   => Mage::helper('mageplaza_actionslist')->__('Meta'),
                'title'   => Mage::helper('mageplaza_actionslist')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'mageplaza_actionslist/adminhtml_tag_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_tag',
                array(
                    'label'   => Mage::helper('mageplaza_actionslist')->__('Store views'),
                    'title'   => Mage::helper('mageplaza_actionslist')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'mageplaza_actionslist/adminhtml_tag_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        $this->addTab(
            'posts',
            array(
                'label' => Mage::helper('mageplaza_actionslist')->__('Posts'),
                'url'   => $this->getUrl('*/*/posts', array('_current' => true)),
                'class' => 'ajax'
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve tag entity
     *
     * @access public
     * @return Mageplaza_ActionsList_Model_Tag
     * @author Sam
     */
    public function getTag()
    {
        return Mage::registry('current_tag');
    }
}
