<?php
/**
 * Mageplaza_ActionsList extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Mageplaza
 * @package        Mageplaza_ActionsList
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Post admin edit tabs
 *
 * @category    Mageplaza
 * @package     Mageplaza_ActionsList
 * @author      Sam
 */
class Mageplaza_ActionsList_Block_Adminhtml_Post_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Sam
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('post_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('mageplaza_actionslist')->__('Post Information'));
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Mageplaza_ActionsList_Block_Adminhtml_Post_Edit_Tabs
     * @author Sam
     */
    protected function _prepareLayout()
    {
        $post = $this->getPost();
        $entity = Mage::getModel('eav/entity_type')
            ->load('mageplaza_actionslist_post', 'entity_type_code');
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entity->getEntityTypeId());
        $attributes->addFieldToFilter(
            'attribute_code',
            array(
                'nin' => array('meta_title', 'meta_description', 'meta_keywords')
            )
        );
        $attributes->getSelect()->order('additional_table.position', 'ASC');

        $this->addTab(
            'info',
            array(
                'label'   => Mage::helper('mageplaza_actionslist')->__('Post Information'),
                'content' => $this->getLayout()->createBlock(
                    'mageplaza_actionslist/adminhtml_post_edit_tab_attributes'
                )
                ->setAttributes($attributes)
                ->toHtml(),
            )
        );
        $seoAttributes = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setEntityTypeFilter($entity->getEntityTypeId())
            ->addFieldToFilter(
                'attribute_code',
                array(
                    'in' => array('meta_title', 'meta_description', 'meta_keywords')
                )
            );
        $seoAttributes->getSelect()->order('additional_table.position', 'ASC');

        $this->addTab(
            'meta',
            array(
                'label'   => Mage::helper('mageplaza_actionslist')->__('Meta'),
                'title'   => Mage::helper('mageplaza_actionslist')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'mageplaza_actionslist/adminhtml_post_edit_tab_attributes'
                )
                ->setAttributes($seoAttributes)
                ->toHtml(),
            )
        );

        //$this->addTab(
        //    'categories',
        //    array(
        //        'label' => Mage::helper('mageplaza_actionslist')->__('Categories'),
        //        'url'   => $this->getUrl('*/*/categories', array('_current' => true)),
        //        'class' => 'ajax'
        //    )
        //);
        //$this->addTab(
        //    'tags',
        //    array(
        //        'label' => Mage::helper('mageplaza_actionslist')->__('Tags'),
        //        'url'   => $this->getUrl('*/*/tags', array('_current' => true)),
        //        'class' => 'ajax'
        //    )
        //);
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve post entity
     *
     * @access public
     * @return Mageplaza_ActionsList_Model_Post
     * @author Sam
     */
    public function getPost()
    {
        return Mage::registry('current_post');
    }
}
