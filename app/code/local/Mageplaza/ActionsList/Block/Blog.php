<?php

class Mageplaza_ActionsList_Block_Actions extends Mage_Core_Block_Template
{

    public function addToplink()
    {
        $name = Mage::helper('mageplaza_actionslist/config')->getGeneralConfig('name');

        $this->getParentBlock()->addLink(
            $name,
            Mage::helper('mageplaza_actionslist/config')->getActionsRoute(),
            $name,
            true);
    }
}
