<?php
/**
 * Mageplaza_ActionsList extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Mageplaza
 * @package        Mageplaza_ActionsList
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Post widget block
 *
 * @category    Mageplaza
 * @package     Mageplaza_ActionsList
 * @author      Sam
 */
class Mageplaza_ActionsList_Block_Post_Widget_View extends Mage_Core_Block_Template implements
    Mage_Widget_Block_Interface
{
    protected $_htmlTemplate = 'mageplaza_actionslist/post/widget/view.phtml';

    /**
     * Prepare a for widget
     *
     * @access protected
     * @return Mageplaza_ActionsList_Block_Post_Widget_View
     * @author Sam
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $postId = $this->getData('post_id');
        if ($postId) {
            $post = Mage::getModel('mageplaza_actionslist/post')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($postId);
            if ($post->getStatus()) {
                $this->setCurrentPost($post);
                $this->setTemplate($this->_htmlTemplate);
            }
        }
        return $this;
    }
}
