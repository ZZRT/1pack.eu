<?php
/**
 * Mageplaza_ActionsList extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Mageplaza
 * @package        Mageplaza_ActionsList
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Category Posts list block
 *
 * @category    Mageplaza
 * @package     Mageplaza_ActionsList
 * @author      Sam
 */
class Mageplaza_ActionsList_Block_Category_Post_List extends Mageplaza_ActionsList_Block_Post_List
{
    /**
     * initialize
     *
     * @access public
     * @return void
     * @author Sam
     */
    public function __construct()
    {
        parent::__construct();
        $category = $this->getCategory();
         if ($category) {
             $this->getPosts()->addCategoryFilter($category->getId());
             $this->getPosts()->unshiftOrder('related_category.position', 'ASC');
         }
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Mageplaza_ActionsList_Block_Category_Post_List
     * @author Sam
     */
    protected function _prepareLayout()
    {
        return $this;
    }

    /**
     * get the current category
     *
     * @access public
     * @return Mageplaza_ActionsList_Model_Category
     * @author Sam
     */
    public function getCategory()
    {
        return Mage::registry('current_category');
    }
}
