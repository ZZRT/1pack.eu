<?php

class Gurusmart_YandexMarket_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();

        $formExportBlock = $this->getLayout()
            ->createBlock('Gurusmart_YandexMarket/adminhtml_export');
        $this->_addContent($formExportBlock);

        $this->renderLayout();
    }

    public function exportAction()
    {
        // Check request
        $exportOnlyMarked = $this->getRequest()->getParam('export-marked', null) === 'on';
        $isDebug = $this->getRequest()->getParam('export-debug', null) === 'on';

        // Get current currency code
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

        // Attributes to select (for products collection)
        $attributesToSelect = array(
            'sku',
            'price',
            'name',
            'manufacturer',
            'url_key',
            'short_description',
            'image',
            'country_of_manufacture'
        );

        $forbiddenCategories = [
            268,
            269,
            270,
            276,
            279,
            249, // Brands.
        ];
        // Get categories collection
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->setOrder('entity_id', 'DESC')
            ->addAttributeToSelect('name');

        // Array of product ids to control uniq
        $productIds = array();
        $categoriesArray = [];
        $offersArray = [];
        // For each category do
        foreach ($categories as $_category) {
            // Cut off forbidden categories.
            if (count(array_intersect(explode(DS, $_category->getPath()), $forbiddenCategories))) continue;
            //if (!count(array_intersect(explode(DS, $_category->getPath()), [213]))) continue;
            if ($_category->getId() < 3) continue;
            // Put category data into result array
            if (2 == $_category->getParentId()) {
                $categoriesArray[] = [
                    'name' => 'category',
                    'attributes' => [
                        'id' => $_category->getId(),
                    ],
                    'value' => $_category->getName(),
                ];
            } else {
                $categoriesArray[] = [
                    'name' => 'category',
                    'attributes' => [
                        'id' => $_category->getId(),
                        'parentId' => $_category->getParentId(),
                    ],
                    'value' => $_category->getName(),
                ];
            }

            // Get products collection and add attributes to select
            $products = $_category->getProductCollection();
            //foreach ($attributesToSelect as $ats) {
            //    $products->addAttributeToSelect($ats);
            //}
            if ($exportOnlyMarked) {
                $products->addAttributeToFilter('xml_export', array('Yes' => true));
            }

            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

            // For each product do
            foreach ($products as $_product) {

                // Check uniq
                if (in_array($_product->getId(), $productIds)) {
                    continue;
                } else {
                    $productIds[] = $_product->getId();
                }
                $_product = Mage::getModel('catalog/product')->load($_product->getId());
                $productBody = [];

                // product suffix
                $helper = Mage::helper('catalog/product');
                $suffix = $helper->getProductUrlSuffix(); // may have particular store ID

                // ------------  url
                $this->addNameValuePair($productBody, 'url', str_replace('/index.php', null, Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_WEB, true )) . $_product->getUrlKey() . $suffix);
                // ------------  price
                if ($_product->getFinalPrice() == $_product->getPrice()) {
                    $this->addNameValuePair($productBody, 'price', $_product->getPrice());
                } else {
                    $this->addNameValuePair($productBody, 'price', $_product->getFinalPrice());
                    $this->addNameValuePair($productBody, 'oldprice', $_product->getPrice());
                }
                
                // ------------  currencyId
                $this->addNameValuePair($productBody, 'currencyId', $currencyCode);
                // ------------  categoryId
                $this->addNameValuePair($productBody, 'categoryId', $_category->getId());
                // ------------  name
                $this->addNameValuePair($productBody, 'name', str_replace('&', '', $_product->getName()));

                // ------------  picture
                $imageCounter = 0;
                foreach ($_product->getMediaGalleryImages() as $image) {
                    //$picUrl = (string)Mage::getModel('catalog/product_media_config')->getMediaUrl($image);
                    $this->addNameValuePair($productBody, 'picture', $image->getUrl());
                    
                    if (++$imageCounter > 9) break;
                }

                // ------------  vendor
                $this->addNameValuePair($productBody, 'vendor', trim($_product->getAttributeText('brand')));
                // ------------  vendorCode
                $this->addNameValuePair($productBody, 'vendorCode', $_product->getSku());

                // ------------  description
                //$productBody['description'] = trim(strip_tags($_product->getShortDescription()));
                //$prdDescription = trim(html_entity_decode(strip_tags(preg_replace('/\s+/', ' ', str_replace('Доставка по Украине БЕСПЛАТНО!', '', $_product->getDescription())))));
                $prdDescription = preg_replace('/\s+/', ' ', preg_replace('~\x{00a0}~siu', ' ', trim(html_entity_decode(strip_tags(str_replace('Доставка по Украине БЕСПЛАТНО!', '', $_product->getDescription()))))));
                $prdDescription = mb_substr($prdDescription, 0, 175, 'UTF-8');
                $prdDescription = mb_substr($prdDescription, 0, mb_strrpos($prdDescription, ' ', 0, 'UTF-8'), 'UTF-8');
                $this->addNameValuePair($productBody, 'description', trim($prdDescription));

                // ------------  delivery
                $this->addNameValuePair($productBody, 'delivery', 'true');
                
                // ------------  country_of_origin
                $this->addNameValuePair($productBody, 'country_of_origin', trim($_product->getAttributeText('country_of_manufacture')));
                
                // ------------  local_delivery_cost
                if ($_product->getFinalPrice() > 500) {
                    $this->addNameValuePair($productBody, 'local_delivery_cost', '0.00');
                }
                
                // Save product data into offers array
                $offersArray[] = [
                    'name' => 'offer',
                    'attributes' => [
                        'id' => $_product->getId(),
                        'available' => (bool)Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getIsInStock(),
                    ],
                    $productBody
                ];
            }
        }

        // Result array
        $resultArray = [
            'name' => 'yml_catalog',
            'attributes' => [
                'date' => date('Y-m-d h:i'),
            ],
            [
                'name' => 'shop',
                [
                    'name' => 'name',
                    'value' => Mage::getModel('core/store')->load(1)->getName(),
                ],
                [
                    'name' => 'company',
                    'value' => Mage::getModel('core/store')->load(1)->getName(),
                ],
                [
                    'name' => 'url',
                    'value' => Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_WEB, true ),
                ],
                [
                    'name' => 'platform',
                    'value' => 'Magento',
                ],
                [
                    'name' => 'currencies',
                    [
                        'name' => 'currency',
                        'attributes' => [
                            'id' => $currencyCode,
                            'rate' => '1',
                        ],
                    ],
                ],
                [
                    'name' => 'categories',
                    $categoriesArray,
                ],
                [
                    'name' => 'offers',
                    $offersArray
                ]
            ],
        ];

if ($isDebug) {
header("Content-Type: text/xml");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0");
header("Cache-Control: max-age=0");
header("Pragma: no-cache");

$doc = new DOMDocument('1.0', 'utf-8');
$child = $this->generate_xml_element( $doc, $resultArray );
if ( $child )
    $doc->appendChild( $child );
$doc->formatOutput = true; // Add whitespace to make easier to read XML
$xml = $doc->saveXML();
echo $xml; die;
}

        try {
        
            $doc = new DOMDocument('1.0', 'utf-8');
            $child = $this->generate_xml_element( $doc, $resultArray );
            if ( $child )
                $doc->appendChild( $child );
            $doc->formatOutput = true; // Add whitespace to make easier to read XML
            $xml = $doc->saveXML();
            
            $xml_doctype_cheater = <<<CHEATER
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
CHEATER;

            // Write result into file
            $path = Mage::getBaseDir('base') . DS . 'products.xml';
            if (!file_put_contents($path, str_replace('<?xml version="1.0" encoding="utf-8"?>', $xml_doctype_cheater, $xml))) {

                $this->_getSession()->addError(
                    $this->__('Can`t write to products.xml')
                );

            }
            // Success
            $this->_getSession()->addSuccess($this->__('Data successfully exported'));
        } catch(Exception $e) {
            // Error occured
            $this->_getSession()->addError($this->__('Can`t process data, please upgrade extension to the latest version'));
        }

        $this->_redirect('*/*');
    }

    private function _processOnRemoteServer(array $result)
    {
        $context = stream_context_create(
            array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
                    'content' => http_build_query(
                        array(
                            'data' => base64_encode(gzencode(serialize($result), $level = 6))
                        )
                    )
                )
            )
        );

        return file_get_contents(
            $file = 'http://www.gurusmart.ru/converter/convert.php',
            $use_include_path = false,
            $context
        );
    }
    
    private function addNameValuePair(&$arrayToAddTo, $name, $value)
    {
        if (!empty($value)) {
            $arrayToAddTo[] = [
                'name' => $name,
                'value' => $value,
            ];
        }
    }
    
    private function generate_xml_element( $dom, $data )
    {
        if ( empty( $data['name'] ) )
            return false;
     
        // Create the element
        $element_value = ( ! empty( $data['value'] ) ) ? $data['value'] : null;
        if (true === is_bool($element_value)) $element_value = $element_value ? 'true' : 'false';
        $element = $dom->createElement( $data['name'], $element_value );
     
        // Add any attributes
        if (!empty( $data['attributes'] ) && is_array($data['attributes'])) {
            foreach($data['attributes'] as $attribute_key => $attribute_value) {
                if (true === is_bool($attribute_value)) $attribute_value = $attribute_value ? 'true' : 'false';
                $element->setAttribute( $attribute_key, $attribute_value );
            }
        }
     
        // Any other items in the data array should be child elements
        foreach ( $data as $data_key => $child_data ) {
            if ( ! is_numeric( $data_key ) ) continue;
            if ( empty( $child_data['name'] ) ) {
                // One level deeper.
                foreach ( $child_data as $child_data_key => $child_child_data ) {
                    if (!is_numeric( $child_data_key)) continue;
                    $child = $this->generate_xml_element( $dom, $child_child_data );
                    if ( $child )
                        $element->appendChild( $child );
                }
            } else {
                $child = $this->generate_xml_element( $dom, $child_data );
                if ( $child )
                    $element->appendChild( $child );
            }
        }
     
        return $element;
    }
}
