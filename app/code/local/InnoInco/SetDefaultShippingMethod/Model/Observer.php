<?php

class InnoInco_SetDefaultShippingMethod_Model_Observer
{
    /**
     * Change product meta title on product view
     *
     * @pram Varien_Event_Observer $observer
     * @return Yourcompany_Yourmodule_Model_Observer
     */
	
    private function _log($string)
    {
		if(Mage::getIsDeveloperMode())
		{
			Mage::log($string, null, 'shipp.log');
		}
	}	
	
    public function setDefaultShippingMethod(Varien_Event_Observer $observer)
    {
		try {
			$quote         =  $observer->getEvent()->getQuote();
			$quoteid       =  $quote->getId();
			$specialDiscountAmount	= 0;
			$cartProductsPrices	= [];
			
			if (!$quoteid) return $this;
			
			$shippingAddress = $quote->getShippingAddress();
			/*
			if (!$shippingAddress->getCountryId()) {
				$shippingAddress->setCountryId('PL')->setShippingMethod('freeshipping_freeshipping')->save();
				$shippingAddress->save();
			}
			*/
			if (!$shippingAddress->getShippingMethod()) {
				$shippingAddress->setShippingMethod('freeshipping_freeshipping')->save();
			}
    
		} catch(Exception $e) {
			Mage::logException($e);
		}
		return $this;
	}
}
