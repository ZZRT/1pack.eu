<?php

class InnoInco_AddCustomerTabInMyAccount_ControllerController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
			$this->_redirect('customer/account/login');
			return;
		}
		
		$this->loadLayout();
		//$this->_initLayoutMessages('catalog/session');
		//$this->_initLayoutMessages('checkout/session');
		$this->renderLayout();
    }
}
