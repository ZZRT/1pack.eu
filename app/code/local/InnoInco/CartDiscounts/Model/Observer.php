<?php

class InnoInco_CartDiscounts_Model_Observer
{
    /**
     * Change product meta title on product view
     *
     * @pram Varien_Event_Observer $observer
     * @return Yourcompany_Yourmodule_Model_Observer
     */
	
    private function _log($string)
    {
		if(Mage::getIsDeveloperMode())
		{
			Mage::log($string, null, 'manuf.log');
		}
	}	
	
    public function setUnusualDiscounts(Varien_Event_Observer $observer)
    {
		try {
			$quote         =  $observer->getEvent()->getQuote();
			$quoteid       =  $quote->getId();
			$specialDiscountAmount	= 0;
			$cartProductsPrices	= [];
			
			if (!$quoteid) return $this;

			$ruleIdsList = $quote->getAppliedRuleIds();
			if ('' != $ruleIdsList) {
				foreach(explode(',',$ruleIdsList) as $ruleId) {
					$rule = Mage::getModel('salesrule/rule')->load($ruleId);
					
					if ('discount_50_on_every_second_bracket' != $rule->getName()) {
						$this->_log('Skipping rule ' . $rule->getName());
					} else {
						$categoriesAndManufacturers = $this->_getCategoriesAndManufacturers($rule);
$this->_log(print_r($rule, true));
$this->_log(print_r($categoriesAndManufacturers, true));
						foreach($quote->getAllItems() as $item){
							$_product = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
							if (!($_product->isVisibleInCatalog() && $_product->isVisibleInSiteVisibility())) continue;
							if (in_array($_product->getBrand(), $categoriesAndManufacturers['manufacturers'])) {
								$commonCategories = array_intersect($_product->getCategoryIds(), explode(',', str_replace(' ', '', $categoriesAndManufacturers['categories'])));
								if (sizeof($commonCategories) > 0) {
									$cartProductsPrices = array_merge($cartProductsPrices, array_fill(0, $item->getQty(), $item->getPriceInclTax()));
								}
							}
						}
						
						sort($cartProductsPrices);
$this->_log(print_r($cartProductsPrices, true));
						for( $i = 0; $i < intval(count($cartProductsPrices) / 2); $i++) {
							//50%
							//$specialDiscountAmount += $cartProductsPrices[$i] / 2;
							//35%
							$specialDiscountAmount += (double)$cartProductsPrices[$i] * 0.35;
$this->_log("i = $i, price = " . $cartProductsPrices[$i] . " specialDiscountAmount = $specialDiscountAmount"  );
						}

						if($specialDiscountAmount > 0) {

							$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
							$currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

							if ($baseCurrencyCode != $currentCurrencyCode) {
								// convert specialDiscountAmount from current currency to base currency
								// $baseSpecialDiscountAmount = Mage::helper('directory')->currencyConvert($specialDiscountAmount, $currentCurrencyCode, $baseCurrencyCode);
								// Allowed currencies
								$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
								$rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
								// the price converted
								$baseSpecialDiscountAmount= $specialDiscountAmount/$rates[$currentCurrencyCode];
								
							} else {
								$baseSpecialDiscountAmount = $specialDiscountAmount;
							}
$this->_log("!!!!baseSpecialDiscountAmount = $baseSpecialDiscountAmount");
							$rule->setDiscountAmount($baseSpecialDiscountAmount)->save();
						}
						if(false) {
						//if($specialDiscountAmount > 0) {
							$total=$quote->getBaseSubtotal();
							$quote->setSubtotal(0);
							$quote->setBaseSubtotal(0);
							$quote->setSubtotalWithDiscount(0);
							$quote->setBaseSubtotalWithDiscount(0);
							$quote->setGrandTotal(0);
							$quote->setBaseGrandTotal(0);
							$canAddItems = $quote->isVirtual()? ('billing') : ('shipping'); 
							foreach ($quote->getAllAddresses() as $address) {
$this->_log('Next address: ' . $address->getAddressType());
								$address->setSubtotal(0);
								$address->setBaseSubtotal(0);
								$address->setGrandTotal(0);
								$address->setBaseGrandTotal(0);
								$address->collectTotals();

								if($address->getAddressType()==$canAddItems) {
$this->_log('Changing address.');
$this->_log('$address->setSubtotalWithDiscount(' . ((float) $address->getSubtotalWithDiscount()-$specialDiscountAmount) . ')');
									$address->setSubtotalWithDiscount((float)$address->getSubtotalWithDiscount()-$specialDiscountAmount);
$this->_log('$address->setGrandTotal(' . ((float) $address->getGrandTotal()-$specialDiscountAmount) . ')');
									$address->setGrandTotal((float) $address->getGrandTotal()-$specialDiscountAmount);
$this->_log('$address->setBaseSubtotalWithDiscount(' . ((float) $address->getBaseSubtotalWithDiscount()-$baseSpecialDiscountAmount) . ')');
									$address->setBaseSubtotalWithDiscount((float)$address->getBaseSubtotalWithDiscount()-$baseSpecialDiscountAmount);
$this->_log('$address->setBaseGrandTotal(' . ((float) $address->getBaseGrandTotal()-$baseSpecialDiscountAmount) . ')');
									$address->setBaseGrandTotal((float)$address->getBaseGrandTotal()-$baseSpecialDiscountAmount);
									if($address->getDiscountDescription()){
$this->_log('Changing address discount description.');
										$address->setDiscountAmount($address->getDiscountAmount()-$specialDiscountAmount);
										//$address->setDiscountDescription($address->getDiscountDescription().', -50% second bracket discount');
										$address->setBaseDiscountAmount(-($address->getBaseDiscountAmount()-$baseSpecialDiscountAmount));
									}else {
$this->_log('Setting address discount description.');
										$address->setDiscountAmount(-($specialDiscountAmount));
										//$address->setDiscountDescription('-50% second bracket discount');
										$address->setBaseDiscountAmount(-($baseSpecialDiscountAmount));
									}
									$address->save();
								}//end: if

								$quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
								$quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());
								$quote->setSubtotalWithDiscount((float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount());
								$quote->setBaseSubtotalWithDiscount((float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount());
								$quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
								$quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());
								$quote ->save();
								
								/*
								$quote->setGrandTotal($quote->getBaseSubtotal()-$specialDiscountAmount)
										->setBaseGrandTotal($quote->getBaseSubtotal()-$specialDiscountAmount)
										->setSubtotalWithDiscount($quote->getBaseSubtotal()-$specialDiscountAmount)
										->setBaseSubtotalWithDiscount($quote->getBaseSubtotal()-$specialDiscountAmount)
										->save();
								
$this->_log('...');
$this->_log('$quote->GrandTotal = ' . $quote->getGrandTotal());
$this->_log('$quote->BaseGrandTotal = ' . $quote->getBaseGrandTotal());
$this->_log('$quote->SubtotalWithDiscount = ' . $quote->getSubtotalWithDiscount());
$this->_log('$quote->BaseSubtotalWithDiscount = ' . $quote->getBaseSubtotalWithDiscount());
$this->_log('...');
								*/

							} //end: foreach
							/*
							foreach($quote->getAllItems() as $item){
								$rat=$item->getPriceInclTax()/$total;
								$ratdisc=$specialDiscountAmount*$rat;
								$item->setDiscountAmount(($item->getDiscountAmount()+$ratdisc) * $item->getQty());
								$item->setBaseDiscountAmount(($item->getBaseDiscountAmount()+$ratdisc) * $item->getQty())->save();
							}
							*/
						}
					}
				}
			}
		} catch(Exception $e) {
			Mage::logException($e);
		}
		return $this;
	}

    private function _getCategoriesAndManufacturers ($rule)
    {
		$result = [
			'categories' => "",
			'manufacturers' => [],
		];
		
		foreach(unserialize($rule->getConditionsSerialized())['conditions'] as $nextFirstLevelRule) {
			// On the first level we are interested only in ONE condition.
			if ('salesrule/rule_condition_product_subselect' != $nextFirstLevelRule['type']
				|| 'qty' != $nextFirstLevelRule['attribute']
				|| '>' != $nextFirstLevelRule['operator']
				|| '1' != $nextFirstLevelRule['value'] ) {Mage::log('Skipping' . print_r($nextFirstLevelRule, true), null, 'manuf.log'); continue;}
			
			foreach($nextFirstLevelRule['conditions'] as $nextRule) {
				// Second level
				if (
						'salesrule/rule_condition_product' == $nextRule['type']
					&&	'category_ids' == $nextRule['attribute']
					&&	'()' == $nextRule['operator']
				) {
					$result['categories'] = $nextRule['value'];
				} elseif (
					'salesrule/rule_condition_product_combine' == $nextRule['type']
					&&	'any' == $nextRule['aggregator']
				) {
					foreach($nextRule['conditions'] as $nextManufacturer) {
						// Third (last) level.
						if (
								'salesrule/rule_condition_product' == $nextManufacturer['type']
							&&	'brand' == $nextManufacturer['attribute']
							&&	'==' == $nextManufacturer['operator']
						) {
							$result['manufacturers'][] = $nextManufacturer['value'];
						}
					}
				}
			}
		}
		return($result);
	}
}
