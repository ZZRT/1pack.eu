<?php

class InnoInco_CatalogSearch_Helper_Data extends Mage_CatalogSearch_Helper_Data
{
	const MAX_RESULT_LEN = 5;
	function getMaxResultLength() {return self::MAX_RESULT_LEN;}
}
