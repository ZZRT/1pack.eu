<?php
class InnoInco_CatalogSearch_Model_Query extends Mage_CatalogSearch_Model_Query
{
	public function getSuggestCollection()
	{
		$resultCollection = $this->getData('suggest_collection');
		if (is_null($resultCollection)) {
			
			$resultCollection = [];
			$keywords = explode(' ', $this->getQueryText());
			
			// Step one: Find EXACT matches.
			$this->_exactFindInTextFieldAndAddToResultCollection($resultCollection, 'sku', $this->getQueryText());
			$this->_exactFindInTextFieldAndAddToResultCollection($resultCollection, 'name', $this->getQueryText());
			$this->_exactFindInTextFieldAndAddToResultCollection($resultCollection, 'short_description', $this->getQueryText());
			$this->_exactFindInTextFieldAndAddToResultCollection($resultCollection, 'description', $this->getQueryText());

			// Step two: SKU.
			foreach($keywords as $nextKeyword) {
				if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
				
				$collection = Mage::getModel('catalog/product')->getCollection();
				$collection
					->addAttributeToSelect(['sku', 'name', 'price', 'short_description', 'description', 'thumbnail'])
					->addAttributeToFilter( [
						['attribute' => 'sku', 'like' => '%' . $nextKeyword . '%'],
						])
					->setPageSize(count($resultCollection) + Mage::helper('innoinco_catalogsearch')->getMaxResultLength())
					->addStoreFilter($this->getStoreId())
				;
					//->addAttributeToFilter('sku', array('like' => '%'.$nextKeyword.'%'))
				Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
				Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
				Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);
				
				foreach($collection as $nextItem) {
					if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
					if (array_key_exists($nextItem->getSku(), $resultCollection)) continue;
					
					$resultCollection[$nextItem->getSku()] = $nextItem;
	Mage::log("Found sku: " . $nextItem->getSku() . " by key: [$nextKeyword]", null, 'search.log', true);
		
				}
			}
			
			// Step two: Name.
			$this->_findInTextFieldAndAddToResultCollection($resultCollection, 'name', $keywords);
			$this->_findInTextFieldAndAddToResultCollection($resultCollection, 'short_description', $keywords);
			$this->_findInTextFieldAndAddToResultCollection($resultCollection, 'description', $keywords);
			
			// Step LAST: Add standard search result.
			$this->_getStandartSearchResultAndAddToResultCollection($resultCollection, $this->getQueryText());
			
			$this->setData('suggest_collection', $resultCollection);
		}
		return $resultCollection;
	}
	
	// *******************************************************************************
	protected function _exactFindInTextFieldAndAddToResultCollection(& $resultCollection, $textField, $exactPhrase)
	{
		// Part ZERO: Exact match.
		if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) return;
		
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection
			->addAttributeToSelect(['sku', 'name', 'price', 'short_description', 'description', 'thumbnail'])
			->setPageSize(count($resultCollection) + Mage::helper('innoinco_catalogsearch')->getMaxResultLength())
			->addStoreFilter($this->getStoreId())
		;
		$collection->addAttributeToFilter( [
			['attribute' => $textField, 'like' => '%' . $exactPhrase . '%'],
			]);
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);
		
		foreach($collection as $nextItem) {
			if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
			if (array_key_exists($nextItem->getSku(), $resultCollection)) continue;
			
			$resultCollection[$nextItem->getSku()] = $nextItem;
		}
	}
	
	// *******************************************************************************
	protected function _findInTextFieldAndAddToResultCollection(& $resultCollection, $textField, $keywords)
	{
		// Part one: Beginning of the words.
		if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) return;
		
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection
			->addAttributeToSelect(['sku', 'name', 'price', 'short_description', 'description', 'thumbnail'])
			->setPageSize(count($resultCollection) + Mage::helper('innoinco_catalogsearch')->getMaxResultLength())
			->addStoreFilter($this->getStoreId())
		;
		foreach($keywords as $nextKeyword) {
			$collection->addAttributeToFilter( [
				['attribute' => $textField, 'like' => $nextKeyword.'%'],		// OR
				['attribute' => $textField, 'like' => '% '.$nextKeyword.'%'],
				]);
		}
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);
		
		foreach($collection as $nextItem) {
//Mage::log("2Found in $textField ", null, 'search.log', true);
			if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
			if (array_key_exists($nextItem->getSku(), $resultCollection)) continue;
			
			$resultCollection[$nextItem->getSku()] = $nextItem;
		}
//if (count($collection) > 0) {		
//	Mage::log($collection->getSelect()->__toString(), null, 'search.log', true);
//	Mage::log("************************************", null, 'search.log', true);
//}
//Mage::log('Beg - ' . print_r($resultCollection, true), null, 'search.log', true);

		// Part two: Any part of the words.
		if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) return;
		
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection
			->addAttributeToSelect(['sku', 'name', 'price', 'thumbnail'])
			->setPageSize(count($resultCollection) + Mage::helper('innoinco_catalogsearch')->getMaxResultLength())
			->addStoreFilter($this->getStoreId())
		;
		foreach($keywords as $nextKeyword) {
			//$collection->addAttributeToFilter($textField, array('like' => '%'.$nextKeyword.'%'));	// AND
			$collection->addAttributeToFilter( [
				['attribute' => $textField, 'like' => '%' . $nextKeyword . '%'],
				]);
		}
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
		Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);
		
		foreach($collection as $nextItem) {
//Mage::log("3Found in $textField ", null, 'search.log', true);
			if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
			if (array_key_exists($nextItem->getSku(), $resultCollection)) continue;
			
			$resultCollection[$nextItem->getSku()] = $nextItem;
		}
//if (count($collection) > 0) {		
//	Mage::log($collection->getSelect()->__toString(), null, 'search.log', true);
//	Mage::log("************************************", null, 'search.log', true);
//}
//Mage::log('Any - ' . print_r($resultCollection, true), null, 'search.log', true);

	}
	
	// *******************************************************************************
	protected function _getStandartSearchResultAndAddToResultCollection(& $resultCollection, $queryText)
	{
		$query = Mage::getModel('catalogsearch/query')->setQueryText($queryText)->prepare();
		$fulltextResource = Mage::getResourceModel('catalogsearch/fulltext')->prepareResult(
			Mage::getModel('catalogsearch/fulltext'),
			$queryText,
			$query
		);

		//Mage::log(print_r($fulltextResource, true), null, 'search.log', true);

		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection
			->addAttributeToSelect(['sku', 'name', 'price', 'short_description', 'description', 'thumbnail'])
			->getSelect()->joinInner(
			array('search_result' => $collection->getTable('catalogsearch/result')),
				$collection->getConnection()->quoteInto(
				'search_result.product_id=e.entity_id AND search_result.query_id=?',
				$query->getId()
			),
			array('relevance' => 'relevance')
		);

		foreach($collection as $nextItem) {
			if (count($resultCollection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) break;
			if (array_key_exists($nextItem->getSku(), $resultCollection)) continue;
			
			$resultCollection[$nextItem->getSku()] = $nextItem;
		}
		
		//if (count($collection) >= Mage::helper('innoinco_catalogsearch')->getMaxResultLength()) {
			$resultCollection[""] = count($collection);
		//}
	}
}
