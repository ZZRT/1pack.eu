<?php
class InnoInco_CatalogSearch_Block_Autocomplete extends Mage_CatalogSearch_Block_Autocomplete
{
    protected function _toHtml()
    {
        $html = '';

        if (!$this->_beforeToHtml()) {
            return $html;
        }

		$suggestCollection = array_values($this->helper('catalogsearch')->getSuggestCollection());
        if (!($count = count($suggestCollection))) {
            return $html;
        }

        $count--;

        $html = '<ul><li style="display:none"></li>';
        $index = 0;
		foreach ($suggestCollection as $item) {
			if (is_int($item)) {
				$html .=
					'<li title="'.$this->getQueryText().'" class="total-search-counter">'
						. $this->__('Get all results (%d)', $item)
					. '</li>';
			} else {
				$html .=
					'<li title="'.$this->escapeHtml($item->getDescription()).'" class="' . (($index%2) ? 'odd' : 'even') . ' ' . (($index == 0) ? 'first' : (($index == $count) ? 'last' : '')) . '">'
						. '<a href="' . $item->getProductUrl() . '">'
							. '<img src="' . Mage::helper('catalog/image')->init($item, 'thumbnail')->resize(44) . '"/>'
							. $this->escapeHtml($item->getName())
							. '<p>' . Mage::helper('core')->currency($item->getPrice(), true, false) . '</p>'
						. '</a>'
					. '</li>';
			}
			//if (Mage::helper('innoinco_catalogsearch')->getMaxResultLength() == ++$index) break;
        }

        $html.= '</ul>';

        return $html;
    }
}
