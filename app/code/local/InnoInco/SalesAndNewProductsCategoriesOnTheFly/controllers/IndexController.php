<?php
class InnoInco_SalesAndNewProductsCategoriesOnTheFly_IndexController extends Mage_Core_Controller_Front_Action
{
	private static $categories = [
			'newproducts' => 268,
			'bestsellers' => 269,
			'sales' => 270,
			];
			
	public function indexAction() {
		$categoryKey = $this->getRequest()->getParam('category', null);

		if (is_string($categoryKey) && array_key_exists($categoryKey, self::$categories)) {
			$categoryId = self::$categories[$categoryKey];
			$url = Mage::getModel('catalog/category')->load($categoryId);
			$this->_redirectUrl(Mage::getModel('catalog/category')->load($categoryId)->getUrl());
		} else {
			$this->_redirectUrl(Mage::getBaseUrl());
		}
	}
}
