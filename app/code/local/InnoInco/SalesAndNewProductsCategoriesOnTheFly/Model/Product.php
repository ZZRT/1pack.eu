<?php
class InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Product extends Mage_Catalog_Model_Product
{
	public function setOrigData($key = null, $data = null)
	{
		if (is_null($key)) {
			$this->_origData = $this->_data;
		} else {
			$this->_origData[$key] = $data;
		}
		return $this;
	}
}
