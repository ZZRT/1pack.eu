<?php
class InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Category_Reset extends Mage_Core_Model_Abstract
{
	const LOCK_ID = 'SalesAndNewProductsCategoriesOnTheFly_CategoryReset';

	private $collection;
	private $indexer;
	
	public function __construct(InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Collection $_collection)
	{
		parent::__construct();
		
		$this->collection = $_collection;
		
		$this->indexer = new Mage_Index_Model_Process();
		$this->indexer->setId(self::LOCK_ID);
	}
	
	private function _log($string)
	{
		if (Mage::getIsDeveloperMode()){
			Mage::log(get_class($this) . ' ' . $string, null, 'SalesAndNewProductsCategoriesOnTheFly.log');
		}
	}
	
	public function tryToResetGivenCategory($_catId)
	{
		if ($this->indexer->isLocked()) {
			$this->_log('Process is locked 1');
		} else {
			$this->indexer->lockAndBlock();
			$this->_resetCategory($_catId);
			$this->indexer->unlock();
		}
	}
	
	protected function _resetCategory($_catId)
	{
		//sleep(11);
		// Getting new product ids.
		$desiredProductIds = $this->collection->getCollection()->getAllIds();
		
		// Getting category product ids.
		$category = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getStoreId())->load($_catId);
		//$categoryProductsPosition = $category->getProductsPosition();	// Does not work on flat categories.
		$categoryProductIds = array_keys(Mage::getResourceModel('catalog/category')->getProductsPosition($category));
		
		$idsToRemove = array_diff($categoryProductIds, $desiredProductIds);
		$idsToAssign = array_diff($desiredProductIds, $categoryProductIds);
		
		array_walk($idsToRemove, function($_productId, $key, $_catId) {
			//Mage::getSingleton('catalog/category_api')->removeProduct($_catId, $_productId);	// Seems like is does not work with flat categories.
			echo "Removing product $_productId from category $_catId <br/>";
			$product = Mage::getModel('categoriesonthefly/product')->setStoreId(Mage::app()->getStore()->getStoreId())->load($_productId);
			$productCategoryIds = $product->getCategoryIds();
			if (($keyToRemove = array_search($_catId, $productCategoryIds)) !== false) unset($productCategoryIds[$keyToRemove]);
			$product->setCategoryIds($productCategoryIds);
			$product->save();
		}, $_catId);
		
		array_walk($idsToAssign, function($_productId, $key, $_catId) {
			//Mage::getSingleton('catalog/category_api')->assignProduct($_catId, $_productId);	// Seems like is does not work with flat categories.
			$product = Mage::getModel('categoriesonthefly/product')->setStoreId(Mage::app()->getStore()->getStoreId())->load($_productId);
			$productCategoryIds = $product->getCategoryIds();
			$productCategoryIds[] = $_catId;
			$product->setCategoryIds($productCategoryIds);
			$product->save();
		}, $_catId);
	}
}
