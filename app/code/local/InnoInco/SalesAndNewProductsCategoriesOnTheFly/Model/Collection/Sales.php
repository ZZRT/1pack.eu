<?php
class InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Collection_Sales
	extends Mage_Core_Model_Abstract
	implements InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Collection
{
	public function getCollection()
	{
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $tomorrow = mktime(0, 0, 0, date('m'), date('d')+1, date('y'));
        $dateTomorrow = date('m/d/y', $tomorrow);
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

		$collection->addStoreFilter()
		 ->addAttributeToFilter('special_price', array('gt'=> -1))
		 //->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $todayDate))
		 ->addAttributeToFilter('special_from_date', array('or'=> array(0 => array('date' => true, 'to' => $todayDate), 1 => array('is' => new Zend_Db_Expr('null')))), 'left')
		 ->addAttributeToFilter('special_to_date', array('or'=> array(0 => array('date' => true, 'from' => $dateTomorrow), 1 => array('is' => new Zend_Db_Expr('null')))), 'left')
		 ->setPageSize(99);
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        return $collection;
	}
}
