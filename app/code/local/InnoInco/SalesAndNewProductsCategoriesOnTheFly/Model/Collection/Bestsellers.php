<?php
class InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Collection_Bestsellers
	extends Mage_Core_Model_Abstract
	implements InnoInco_SalesAndNewProductsCategoriesOnTheFly_Model_Collection
{
    public function getCollection()
    {
        $storeId    = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('reports/product_collection')
            //->addAttributeToSelect('*')
			->addOrderedQty()
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder('ordered_qty', 'desc')
    		->setPageSize(30)
    		;
            
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);

        return $products;
    }
}
