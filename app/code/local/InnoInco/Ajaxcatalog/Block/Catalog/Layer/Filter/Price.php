<?php

class InnoInco_Ajaxcatalog_Block_Catalog_Layer_Filter_Price extends Smartwave_Ajaxcatalog_Block_Catalog_Layer_Filter_Price
{
	/**
	*
	* Prepare html for slider and add JS that incorporates the slider.
	*
	* @return html
	*
	*/
	
	public function getHtml(){		
		if($this->getSliderStatus()){
			$text='<div class="price price-filter-slider">
				<div>
					<div id="price-range-slider" class="slider-range"></div>
                    '.$this->getPriceDisplayType().'
					
				</div><div class="clearer"></div></div>'.$this->getSliderJs();	
			
			return $text;
		} else {
            return parent::_toHtml();
        }	
	}
	
	/*
	* Get JS that brings the slider in Action
	* 
	* @return JavaScript
	*/
	public function getSliderJs(){
		
		$baseUrl = $this->getCurrentUrlWithoutParams();
		$timeout = $this->getConfigTimeOut();
		$styles = $this->prepareCustomStyles();
		
		if($this->isAjaxSliderEnabled()){
			$ajaxCall = 'sliderAjax(url);';
		}else{
			$ajaxCall = 'window.location=url;';
		}
		
		if($this->isTextBoxEnabled()){
			$updateTextBoxPriceJs = '
							// Update TextBox Price
							$(".minPrice").val(newMinPrice); 
							$(".maxPrice").val(newMaxPrice);';
		}
		
		
		$html = '
			<script type="text/javascript">
				jQuery(function($) {
					var newMinPrice, newMaxPrice, url, temp;
					var categoryMinPrice = '.$this->_minPrice.';
					var categoryMaxPrice = '.$this->_maxPrice.';
                    
					function isNumber(n) {
					  return !isNaN(parseFloat(n)) && isFinite(n);
					}
					
					$(".priceTextBox").focus(function(){
						temp = $(this).val();	
					});
					
					$(".priceTextBox").keyup(function(){
						var value = $(this).val();
						if(value!="" && !isNumber(value)){
							$(this).val(temp);	
						}
					});
					
					$(".priceTextBox").keypress(function(e){
						if(e.keyCode == 13){
							var value = $(this).val();
							if(value < categoryMinPrice || value > categoryMaxPrice){
								$(this).val(temp);	
							}
							url = getUrl($(".minPrice").val(), $(".maxPrice").val());
							'.$ajaxCall.'	
						}	
					});
					
					$(".priceTextBox").blur(function(){
						var value = $(this).val();
						if(value < categoryMinPrice || value > categoryMaxPrice){
							$(this).val(temp);	
						}
						
					});
					
					$(".go").click(function(){
						url = getUrl($(".minPrice").val(), $(".maxPrice").val());
						'.$ajaxCall.'	
					});


					var priceSlider = document.getElementById("price-range-slider");
					noUiSlider.create( priceSlider, {
						start:  [ '.$this->getCurrMinPrice().', '.$this->getCurrMaxPrice().' ],
						step: 1,
						range: {
							"min": [ categoryMinPrice ],
							"max": [ categoryMaxPrice ]
						}
					});

					priceSlider.noUiSlider.on("update", function(values, handle){
						newMinPrice = Math.floor(values[0]);
						newMaxPrice = Math.floor(values[1]);

						$( ".price-amount" ).val( "'.$this->getCurrencySymbol().'" + newMinPrice + " - '.$this->getCurrencySymbol().'" + newMaxPrice );

						'.$updateTextBoxPriceJs.'
					});

					priceSlider.noUiSlider.on("set", function(values, handle){
						// Current Min and Max Price
						var newMinPrice = Math.floor(values[0]);
						var newMaxPrice = Math.floor(values[1]);

						// Update Text Price
						$( ".price-amount" ).val( "'.$this->getCurrencySymbol().'"+newMinPrice+" - '.$this->getCurrencySymbol().'"+newMaxPrice );

						'.$updateTextBoxPriceJs.'

						url = getUrl(newMinPrice,newMaxPrice);
						if(newMinPrice != '.$this->getCurrMinPrice().' && newMaxPrice != '.$this->getCurrMaxPrice().'){
						    try {
							    clearTimeout(timer);
							    //window.location= url;
                            } catch(e){}

						}else{
								timer = setTimeout(function(){
									'.$ajaxCall.'
								}, '.$timeout.');
							}
					});

					function getUrl(newMinPrice, newMaxPrice){
						return "'.$baseUrl.'"+"?min="+newMinPrice+"&max="+newMaxPrice+"'.$this->prepareParams().'";
					}
				});
			</script>
			
			'.$styles.'
		';	
		
		return $html;
	}
	
	
	/*
	*
	* Prepare custom slider styles as per user configuration
	*
	* @return style/css
	*
	*/
	
	public function prepareCustomStyles(){
		$useImage = $this->getConfig('ajax_catalog/price_slider_conf/use_image');
		
		$handleHeight = $this->getConfig('ajax_catalog/price_slider_conf/handle_height');
		$handleWidth = $this->getConfig('ajax_catalog/price_slider_conf/handle_width');
		
		$sliderHeight = $this->getConfig('ajax_catalog/price_slider_conf/slider_height');
		$sliderWidth = $this->getConfig('ajax_catalog/price_slider_conf/slider_width');
		
		$amountStyle = $this->getConfig('ajax_catalog/price_slider_conf/amount_style');
		
		$bgHandle = '';
		$bgRange = '';
		$bgSlider = '';

		if($useImage){
			$handle = $this->getConfig('ajax_catalog/price_slider_conf/handle_image');
			$range = $this->getConfig('ajax_catalog/price_slider_conf/range_image');
			$slider = $this->getConfig('ajax_catalog/price_slider_conf/background_image');	
			
			if($handle){$bgHandle = 'url('.$this->_imagePath.$handle.') no-repeat';}
			if($range){$bgRange = 'url('.$this->_imagePath.$range.') repeat-x';}
			if($slider){$bgSlider = 'url('.$this->_imagePath.$slider.') repeat-x';}
		}else{	
			$bgHandle = $this->getConfig('ajax_catalog/price_slider_conf/handle_color');
			$bgRange = $this->getConfig('ajax_catalog/price_slider_conf/range_color');
			$bgSlider = $this->getConfig('ajax_catalog/price_slider_conf/background_color');	
			
		}
		
		$html = '<style type="text/css">';	
			$html .= '.ui-slider .ui-slider-handle{';
			if($bgHandle){$html .= 'background:'.$bgHandle.';';}
			$html .= 'width:'.$handleWidth.'px; height:'.$handleHeight.'px; border: 0; margin-top: -1px; cursor: pointer; border-radius: 5px; }';
			
			$html .= '.ui-slider{';
			if($bgSlider){$html .= 'background:'.$bgSlider.';';}
			$html .= ' width:'.$sliderWidth.'px; height:'.$sliderHeight.'px; border:none; border-radius: 0; -moz-border-radius: 0; -webkit-border-radius: 0; cursor: pointer; margin: 5px 5px 20px 8px; }';
			
			$html .= '.ui-slider .ui-slider-range{';
			if($bgRange){$html .= 'background:'.$bgRange.';';}
			$html .= 'border:none; cursor: pointer; box-shadow: inset 0px 1px 2px 0px rgba(0,0,0,.38); }';
			
			$html .= '#amount{'.$amountStyle.'}';	
		$html .= '</style>';
        //return $html;
        return '';
	}
	
}
