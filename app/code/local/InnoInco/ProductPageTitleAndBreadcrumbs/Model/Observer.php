<?php

class InnoInco_ProductPageTitleAndBreadcrumbs_Model_Observer
{
    /**
     * Change product meta title on product view
     *
     * @pram Varien_Event_Observer $observer
     * @return Yourcompany_Yourmodule_Model_Observer
     */
    public function changeProductPageBreadcrumbs(Varien_Event_Observer $observer)
    {
return $this;
        $layout = $observer->getLayout();

        /**
         * Return, if have not Bradcrubs block
         */
        if (!$layout->getBlock('breadcrumbs')) {
            return $this;
        }

        /**
         * Return, if have not product, not product page
         */
        $currentProduct = Mage::registry('current_product');

        if (!$currentProduct) {
            return $this;
        }
        
        $breadcrumbs_block = $layout->getBlock('breadcrumbs');
        $breadcrumbs_block->unset($breadcrumbs_block->_crumbs['product']);
        
		/**
		 * Add new crumb for current product
		 */
		$breadcrumbs_block->addCrumb('product', array(
			'label' => $currentProduct->getName(),
			'readonly' => false,
			'last' => true
		));
            
        return $this;
    }

}
