<?php
class Ak_NovaPoshta_Block_Checkout_Shipping_Destination
    extends Mage_Core_Block_Template
{
    /**
     * @return Ak_NovaPoshta_Model_Warehouse|bool
     */
    public function getWarehouse()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $warehouseId = $quote->getShippingAddress()->getData('warehouse_id');
        if ($warehouseId) {
            $warehouse = Mage::getModel('novaposhta/warehouse')->load($warehouseId);
            if ($warehouse->getId()) {
                return $warehouse;
            }
        }

        return false;
    }

    /**
     * @return bool|int
     */
    public function getCityId($cityName = '')
    {
        $cityId = $this->getData('city_id');

        if($cityName)
        {
            $cityId = Mage::getModel('novaposhta/city')->load($cityName, 'name_ru')->getId();
            $this->setData('city_id', $cityId);
        }
        
        if (!$cityId) {
            $warehouse = $this->getWarehouse();
            if ($warehouse) {
                $cityId =  $warehouse->getCity()->getId();
                $this->setData('city_id', $cityId);
            }
        }
        
        if ($cityId) {
            return $cityId;
        }

        return false;
    }

    /**
     * @return Ak_NovaPoshta_Model_Resource_City_Collection
     */
    public function getCities()
    {
        $collection = Mage::getResourceModel('novaposhta/city_collection');
        $collection->setOrder('name_ru', Varien_Data_Collection_Db::SORT_ORDER_ASC);
        
        return $collection;
    }

    /**
     * @return Ak_NovaPoshta_Model_Resource_Warehouse_Collection|bool
     */
    public function getWarehouses($cityName = '')
    {
        if ($cityId = $this->getCityId($cityName)) {
            /** @var Ak_NovaPoshta_Model_Resource_Warehouse_Collection $collection */
            $collection = Mage::getResourceModel('novaposhta/warehouse_collection');
            $collection->addFieldToFilter('city_id', $cityId);
            $collection->setOrder('number_in_city', Varien_Data_Collection_Db::SORT_ORDER_ASC);

            return $collection;
        }

        return false;
    }
}