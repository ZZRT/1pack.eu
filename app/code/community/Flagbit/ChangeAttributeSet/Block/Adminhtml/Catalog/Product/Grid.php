<?php

class Flagbit_ChangeAttributeSet_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid {

    protected function _prepareColumns()
    {
        /**
         * Adding URL KEY text column
         */
        $this->addColumn('category_id', array(
            'header'    =>  Mage::helper('catalog')->__('Category'),
            'width'     =>  '100px',
            'index'     =>  'category_id',
            'type'  => 'options',
            'options' => $this->categoriesToOptionArray(),
        ));

        // show URL Key column after SKU column
        $this->addColumnsOrder('category', 'status');

        return parent::_prepareColumns();
    }

    /**
     * Get all categories to option array
     * @return array
     */
    public function categoriesToOptionArray()
    {
        $categories[] = array();
        $categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('url_path')
            ->addAttributeToSort('name', 'asc')
            ->addFieldToFilter('is_active', array('eq'=>'1'))
            ->load()
            ->toArray();

        foreach ($categoriesArray as $categoryId => $category) {
            if($category['parent_id'] > 2){
                if (isset($category['name'])) {
                    $categories[] = array(
                        'value' => $categoryId,
                        'label' => $category['name']
                    );
                }
            }
        }

        return $categories;
    }

    protected function getProductCollectionByCategoryId($category_id)
    {
        $category = Mage::getModel('catalog/category')->load($category_id);
        $products = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addCategoryFilter($category);
        return $products;
    }

    protected function _prepareCollection()
    {
        return parent::_prepareCollection();
    }
}