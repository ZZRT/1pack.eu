if(window.screen.width <= 768){
  //Keep track of how many swipes
  var count=0;
  //Enable swiping...
  var swipeElement = jQuery(".product-img-box");

  swipeElement.swipe( {
    swipeLeft:function(event, direction, distance, duration, fingerCount) {
      jQuery('.etalage-next').click();
    },
    swipeRight:function(event, direction, distance, duration, fingerCount) {
      jQuery('.etalage-prev').click();
    }
  });
}
